from time import time
from Crypto.Hash import SHA256


blockchain = [getGenesisBlock()]

sockets = []
MessageType ={
        'QUERY_LATEST':0,
        'QUERY_ALL':1,
        'RESPONSE_BLOCKCHAIN':2,
        }

class Block:

    def __init__(self, index, previousHash, timestamp, data, hash):
        self.index = index
        self.previousHash = str(previousHash)
        self.tiemstamp = time()
        self.data = data
        self.hash = hash


def calculateHash(index, previousHash, timestamp, data):
    hashsum = str(index) + str(previousHash) + str(timestamp) + str(data)
    return SHA256.new(hashsum.encode()).hexdigest()


def generateNextBlock(blockData):
    previousBlock = getLatestBlock()
    nextIndex = previousBlock.index + 1
    nextTimestamp = time()
    nextHash = calculateHash(
        nextIndex, previousBlock.hash, nextTimestamp, blockData)
    return Block(nextIndex, previousBlock.hash, nextTimestamp, BlockData, nextHash)


def getGenesisBlock():
    return Block(0, '0', 1488870592.2835138, 'hello blockchain', '85f5e10431f69bc2a14046a13aabaefc660103b6de7a84f75c4b96181d03f0b5')


def isValidNewBlock(newBlock, previousBlock):
    if previousBlock.index + 1 != newBlock.index:
        print('invalid index')
        return false

    elif previousBlock.hash != new.Block.previousHash:
        print('invalid previoushash')
        return false

    elif calculateHashForBlock(newBlock) != newBlock.hash:
        print('invalid hash: {} {}'.format(
            calculateHashForBlock(newBlock), newBlock.hash))
        return false

    return true


def replaceChain(newBlocks):
    if isValidNewBlock(newBlocks) and newBlocks.length > blockchain.length:
        print('Received blockchain is valid. Replacing current blockchain with recived blockchain')
        blockchain = newBlocks
        # TODO: May need to add broadcast responselatestmsg()
    else:
        print('Received blockchain invalid')

